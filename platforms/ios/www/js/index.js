/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);

    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var pushNotification = window.plugins.pushNotification;
        pushNotification.register(app.successHandler, app.errorHandler,{"senderID":"125016823459","ecb":"app.onNotificationGCM"});
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
        var args = [];
        var devKey = "9EoyaVgUtezznxXEEqHMX";   // your AppsFlyer devKey
        args.push(devKey);
        var userAgent = window.navigator.userAgent.toLowerCase();
        window.plugins.appsFlyer.initSdk(args);

        window.analytics.startTrackerWithId('UA-62757952-2');
    },
    successHandler: function(result) {
        //alert('Callback Success! Result = '+result);
    },
    errorHandler:function(error) {
        alert(error);
    },
    broadcast: function(data){
        angular.element(document.getElementById('landingController')).scope().broadcast(data);
    },
    redirect: function(data){
        angular.element(document.getElementById('landingController')).scope().redirecturl(data);
    },
    redirectforeground: function(data){
        angular.element(document.getElementById('landingController')).scope().redirectfore(data);
    },
    beepnotif: function(){
        angular.element(document.getElementById('landingController')).scope().beep();
    },
    setreg: function(data){
        angular.element(document.getElementById('landingController')).scope().setregid(data);
    },
    onNotificationGCM: function(e) {
        switch( e.event )
        {
            case 'registered':
                if ( e.regid.length > 0 )
                {
                    app.setreg( e.regid );
                }
                break;

            case 'message':
                if(e.payload.type == 'offline'){
                    if(e.foreground){
                        app.redirect(e.payload.redirect);
                    }else{
                        app.redirectforeground('ask/show/'+e.payload.redirect);
                    }
                }else if(e.payload.type == 'live'){
                    if(e.foreground){
                        app.beepnotif();
                    }else{
                        app.redirectforeground('chats/'+e.payload.redirect);
                    }
                }else if(e.payload.type == 'forum'){
                    if(e.foreground){
                        app.beepnotif();
                    }else{
                        app.redirectforeground('discussions/'+e.payload.redirect+'/1');
                    }
                }else if(e.payload.type == 'articles'){
                    if(e.foreground){
                        app.beepnotif();
                    }else{
                        app.redirectforeground('articles/'+e.payload.redirect);
                    }
                }else if(e.payload.type == 'broadcast'){
                    if(e.foreground){
                        app.broadcast(e.payload.message);
                    }
                }

                break;

            case 'error':
                alert('GCM error = '+e.msg);
                break;

            default:
                alert('An unknown GCM event has occurred');
                break;
        }
    },
};