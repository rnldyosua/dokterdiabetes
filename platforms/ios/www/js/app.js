var soyjoyApp = angular.module('soyjoyApp', [
  'ngRoute',
  'soyjoyControllers'
]);

// var _apiUrl = 'http://192.168.22.10/klix/soyjoy/diabetes/api_old/public/index.php/';
// var _assets = 'http://192.168.22.10/klix/soyjoy/diabetes/api_old/storage/app/';

var _apiUrl = 'http://cleverativebox.com/soyjoy/api/public/index.php/';
var _assets = 'http://cleverativebox.com/soyjoy/api/storage/app/';

//var _apiUrl = 'http://128.199.80.173/api/public/index.php/';
//var _assets = 'http://128.199.80.173/api/storage/app/';

var _apiKey = 'BFAEB6C929F1E445D685B599FDD82';

soyjoyApp.config(['$routeProvider', function($routeProvider){
	$routeProvider.
        when('/',{
            templateUrl : 'pages/landing.html',
            controller : 'landingController'
        }).
		when('/home',{
			templateUrl : 'pages/home.html',
			controller : 'mainController'
		}).
		when('/articles',{
			templateUrl : 'pages/articles.html',
			controller : 'articlesController'
		}).
		when('/articles/:param',{
			templateUrl : 'pages/articledetail.html',
			controller : 'articleViewController'
		}).
        when('/search/article/:category/:query',{
            templateUrl : 'pages/articles.html',
            controller : 'articlesSearchController'
        }).
		when('/discussions',{
			templateUrl : 'pages/discussions.html',
			controller : 'discussionsController'
		}).
		when('/discussions/:id/:page', {
			templateUrl : 'pages/discussiondetail.html',
			controller : 'discussionViewController'
		}).
        when('/search/discussion/:category/:query',{
            templateUrl : 'pages/discussions.html',
            controller : 'discussionSearchController'
        }).
        when('/user/register', {
			templateUrl : 'pages/register.html',
			controller : 'userRegisterController'
		}).
        when('/user/login', {
            templateUrl : 'pages/login.html',
            controller : 'userLoginController'
        }).
        when('/user/forget', {
            templateUrl : 'pages/forget.html',
            controller : 'userForgetController'
        }).
        when('/chats/:page',{
            templateUrl : 'pages/chatlist.html',
            controller : 'chatListController'
        }).
        when('/chat/:doctorId/:token', {
            templateUrl : 'pages/chat.html',
            controller : 'chatController'
        }).
        when('/ask/:doctorid', {
            templateUrl : 'pages/ask.html',
            controller : 'askController'
        }).
        when('/ask/show/:id',{
            templateUrl : 'pages/askdetail.html',
            controller : 'askDetailController'
        }).
        when('/profile',{
            templateUrl : 'pages/profile.html',
            controller : 'profileController'
        }).
        when('/user/password',{
            templateUrl : 'pages/password.html',
            controller : 'passwordController'
        }).
        when('/upload',{
            templateUrl : 'pages/upload.html',
            controller : 'uploadController'
        }).
        when('/buddy',{
            templateUrl : 'pages/diabetesbuddy.html',
            controller : 'diabetesBuddy'
        }).
        when('/mitosfakta',{
            templateUrl : 'pages/mitosfakta.html',
            controller : 'mitosFakta'
        }).

        when('/alfaonline', {
            templateUrl : 'pages/alfa.html',
            controller : 'alfaController'
        }).

        when('/checkup', {
            templateUrl : 'pages/checkup.html',
            controller : 'checkupController'
        }).

        when('/checkupedit', {
            templateUrl : 'pages/checkupedit.html',
            controller : 'checkupeditController'
        }).

        when('/foodrecord', {
            templateUrl : 'pages/foodrecord1.html',
            controller : 'foodRecordController'
        }).

        when('/foodresult', {
            templateUrl : 'pages/foodrecord2.html',
            controller : 'foodResultController'
        }).

        when('/tensirecord/:num', {
            templateUrl : 'pages/tensirecord.html',
            controller : 'tensionController'
        }).
        when('/glucoserecord/:num', {
            templateUrl : 'pages/glucoserecord.html',
            controller : 'glucoseController'
        }).
        when('/editglucose/:date',{
            templateUrl : 'pages/glucoseedit.html',
            controller : 'glucoseEditController'
        }).
        when('/weightrecord/:num', {
            templateUrl : 'pages/weightrecord.html',
            controller : 'weightController'
        }).
        when('/diabetescalculator',{
            templateUrl : 'pages/diabetescal.html',
            controller : 'diabetesController'
        }).
        otherwise({redirectTo: '/'});
}]);
