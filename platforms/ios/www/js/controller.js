var soyjoyControllers = angular.module('soyjoyControllers', ['ui.bootstrap', 'ngSanitize', 'ngCordova']);

soyjoyControllers.run(function($rootScope, $http, $location, $window){
    $rootScope.numpage = 0;
    $rootScope.assets = _assets;
    $rootScope.$on('$routeChangeSuccess', function () {
        $rootScope.pageClass = '';
        var _token = window.localStorage.getItem('token');
        var _userId = window.localStorage.getItem('userId');
        var _regid = window.localStorage.getItem('regid');
        $http.defaults.headers.common['X-CSRF-TOKEN'] = _apiKey;
        $http.defaults.headers.common['USER-TOKEN'] = _token;
        $rootScope.logout = function () {
            $http.get(_apiUrl + 'auth/mobilelogout').
                success(function (data, headers, config, status) {
                    localStorage.removeItem('token');
                    localStorage.removeItem('userId');
                    localStorage.clear();
                    if($location.path() == '/'){
                        $window.location.reload();
                    }else{
                        $location.path('/');
                    }
                })
        }

        $rootScope.backFunction = function(){
            if($location.path() == '/'){
                navigator.app.exitApp();
            }else{
                $window.history.back();
            }
            angular.element('.loading').fadeOut();
        }


        if (_token ) {
            $rootScope.userLogin = 1;
            $rootScope.token = _token;
            if(_userId !== 'undefined'){
                $rootScope.userId = _userId;
            }else{
                $http.get(_apiUrl+'user/'+_token).
                    success(function(data, headers, config, status){
                       window.localStorage.setItem('userId', data.id);
                    });
            }
        } else {
            $rootScope.userLogin = 0;
        }

    });
});

soyjoyControllers.controller('landingController', function($scope, $rootScope, $http, $location){
    if($rootScope.userLogin == 1){
        window.localStorage.setItem('baseurl', $location.absUrl());
        document.addEventListener('deviceready', app.onDeviceReady, false);
        if($location.path() == '/'){
            document.addEventListener('backbutton', $rootScope.backFunction, false);
        }
        $http.get(_apiUrl+'misc/sliders?is_front=0').
            success(function(data){
                $scope.w = window.innerWidth;
                $scope.h = window.innerHeight;
                $scope.myInterval = 5000;
                var slides = $scope.slides = [];
                $scope.addSlide = function() {
                    var newWidth = 600 + slides.length + 1;
                    for(var i = 0; i < data.length; i++){
                        slides.push({
                            image: _assets+data[i].image,
                            text: data[i].description
                        });
                    }
                };
                $scope.addSlide();
            });
    }else if($rootScope.userLogin == 0){
        $http.get(_apiUrl+'misc/sliders').
            success(function(data){
                $scope.w = window.innerWidth;
                $scope.h = window.innerHeight;
                $scope.myInterval = 5000;
                var slides = $scope.slides = [];
                $scope.addSlide = function() {
                    var newWidth = 600 + slides.length + 1;
                    for(var i = 0; i < data.length; i++){
                        slides.push({
                            image: _assets+data[i].image,
                            text: data[i].description
                        });
                    }
                };
                $scope.addSlide();
            });
    }



    $rootScope.pageClass = 'landing';


    $scope.setregid = function(data){
        if($rootScope.userLogin == 1){
            //alert('set reg'+data);
            //alert('regid login '+ localStorage.getItem('userId'));
            $http.post(_apiUrl+'storeregid', {'id': localStorage.getItem('userId'), 'regid': data }).
                success(function(data){
                    //alert('success');
                })
        }

        window.localStorage.setItem('regid', data);
    }
    $scope.redirecturl = function (data) {
        $rootScope.redirectdata = data;
        navigator.notification.confirm(
            'Jawaban anda dijawab oleh dokter kami.', // message
            $scope.onConfirm,            // callback to invoke with index of button pressed
            'Notifikasi',           // title
            ['Buka','Tidak']         // buttonLabels
        );
    };
    $scope.redirectfore = function(data){
        window.location.href = window.localStorage.getItem('baseurl')+data;
    }
    $scope.beep = function(){
        navigator.notification.beep(1);
    }

    $scope.broadcast = function(data){
        navigator.notification.alert(
            data,  // message
            'Message',            // title
            'Notifikasi '                  // buttonName
        );
    }
    $scope.onConfirm = function(buttonIndex){
        if(buttonIndex == 1){
            window.location.href = window.localStorage.getItem('baseurl')+'ask/show/'+$rootScope.redirectdata;
        }
    }

})

soyjoyControllers.controller('mainController', function($scope, $rootScope){
    $rootScope.pageClass = 'home';
    $scope.h = window.innerHeight;
    $scope.assets = _assets;
});

soyjoyControllers.controller('profileController', function($scope, $http, $rootScope, $cordovaCamera, $cordovaFileTransfer){

    $scope.options = ['pria', 'wanita'];

    $http.get(_apiUrl+'profile/'+$rootScope.token).
        success(function(data){
            $scope.form = data;
            console.log(data);
            $scope.imagePhoto = _assets+data.avatar;
        })

    $scope.update = function(form){
        $http.post(_apiUrl+'update/profile',{'id': $rootScope.userId, 'token': $rootScope.token, 'name' : form.name, 'email': form.email, 'hp': form.hp, 'gender': form.gender, 'age': form.age, 'city': form.city}).
            success(function(data){
                alert(data.message);
            }).error(function(data){
                console.log(data);
            })
    }


    $scope.getPicture = function() {
        var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        };
        $cordovaCamera.getPicture(options).then(function(imageURI) {
            var option = {};
            angular.element('.loading').fadeIn();
            $cordovaFileTransfer.upload(_apiUrl+'upload', imageURI, option)
                .then(function(result) {
                    angular.element('.loading').fadeOut();
                    var fileName = result.response;
                    $http.post(_apiUrl+'avatar', {'id': $rootScope.userId, 'avatar': fileName}).
                        success(function(data){
                            $scope.imagePhoto = _assets+fileName;
                        });
                }, function(err) {
                    alert(result)
                }, function (progress) {
                    // constant progress updates
                });

        }, function(err) {
            alert(err);
        });

    }



    $scope.photoUpload = function(){
        var options = new FileUploadOptions();
        var pic = $scope.sPicData;
        options.fileKey = "file";
        options.fileName = pic.substr(pic.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.chunkedMode = false;

        var params = new Object();
        params.fileKey = "file";
        options.params = {}; // eig = params, if we need to send parameters to the server request

        ft = new FileTransfer();
        ft.upload(sPicData, "http://cleverativebox.com/soyjoy/api/public/index.php/upload", win, fail, options);
    }


    function win(){
        alert("image uploaded scuccesfuly");
    }

    function fail(){
        alert("image upload has been failed");
    }

});

soyjoyControllers.controller('passwordController', function($scope, $http, $rootScope, $route){

    $scope.change = function(form){
        $http.post(_apiUrl+'mobile/password', { old: form.old, token : $rootScope.token, newpassword: form.new, confirmpassword: form.confirm }).success(function(data){

            if(data.status == 1){
                alert(data.message);
                setTimeout(function(){
                    $route.reload();
                }, 1000);
            }else if(data.status == 0){
                alert(data.message);
            }else{
                console.log(data.error);
                var error = '';
                $.each(data.error, function(index, value) {
                    error += '<li>'+value+'</li>';
                });
                angular.element('.register-error').html(error);
            }

        }).error(function(data){
            console.log(data);
        })
    }
})

soyjoyControllers.controller('articlesController', function($scope, $http, $location){
    $scope.master = {};
    $scope.page = 1;
    $scope.asset = _assets;
    $http.get(_apiUrl+'article').
        success(function(data, status, headers, config){
            $scope.articles = [data];
            console.log($scope.articles);
            if(data.next_page_url){
                $scope.page++;
            }

        }).error(function(data, status, headers, config){
            console.log(status);
        });
    $scope.push = function(){
        angular.element('.swith-page').css('display', 'none');
        $http.get(_apiUrl+'article?page='+$scope.page).
            success(function(data){
                $scope.articles.push(data);
                if(data.next_page_url){
                    $scope.page++;
                }
            })
    }
    $http.get(_apiUrl+'articlecategory').success(function(data){
        $scope.options = data;
        $scope.selectedOption = $scope.options[0];
    })


    $scope.search = function(form){
        $location.path('/search/article/'+form.category.id_category+'/'+form.query);
    }
});

soyjoyControllers.controller('articlesSearchController', function($scope, $routeParams, $http, $location){
    $http.get(_apiUrl+'search/article/'+$routeParams.category+'/'+$routeParams.query).
        success(function(data){
            $scope.articles = [data];
            console.log(data);
        });

    $http.get(_apiUrl+'articlecategory').success(function(data){
        $scope.options = data;
    })


    $scope.search = function(form){
        $location.path('/search/article/'+form.category.id_category+'/'+form.query);
}
})

soyjoyControllers.controller('articleViewController', function($scope, $routeParams, $http, $rootScope, $route){
    $scope.asset = _assets;
    $scope.externalLink = function($url){
        window.open($url, '_blank', 'location=yes');
    }
    $http.get(_apiUrl+'article/'+$routeParams.param).
        success(function(data, status, headers, config){
            console.log(data);
            if(Object.keys(data).length === 0){
                $scope.result = 0;
            }else{
                $scope.result = data;
                $http.get(_apiUrl+'comment/'+$routeParams.param).
                    success(function(data, status, headers, config){
                       $scope.comments = data;
                    });
            }
        })

    $scope.submit = function(form){
        $http.post(_apiUrl+'comment', {'id': $routeParams.param,'user_id': $rootScope.userId, 'body': form.comment}).
            success(function(data, status, headers, config){
                $route.reload();
            }).
            error(function(data, status,headers, config){
                console.log(data);
            });
    };

});

soyjoyControllers.controller('discussionsController', function($scope, $http, $location, $rootScope, $route){
    $scope.page = 1;
    $http.get(_apiUrl+'discussion').
        success(function(data, status, headers, config){
            $scope.discussions = [data];
            if(data.next_page_url){
                $scope.page++;
            }
        });
    $scope.push = function(){
        angular.element('.swith-page').css('display', 'none');
        $http.get(_apiUrl+'discussion?page='+$scope.page).
            success(function(data){
                $scope.discussions.push(data);
                if(data.next_page_url){
                    $scope.page++;
                }
            })
    }
    $http.get(_apiUrl+'discussioncategory').success(function(data){
        $scope.options = data;        
    });

    $scope.search = function(form){
        $location.path('/search/discussion/'+form.category.id_category+'/'+form.query);
    };

    $scope.submit = function(form){
        angular.element('.loading').fadeIn();
        angular.element('.button-red').css('display','none');
        $http.post(_apiUrl+'discussion/store', {'id_category': form.category.id_category, 'user_id': $rootScope.userId, 'topic': form.topic, 'body': form.body}).
            success(function(data){
                $route.reload();
                angular.element('.loading').fadeOut();
            }).error(function(data){
                $route.reload()
            })
    }

});

soyjoyControllers.controller('discussionViewController', function($route, $scope, $routeParams, $http, $location, $window, $rootScope){    
    $http.get(_apiUrl+'discussion/'+$routeParams.id).
        success(function(data, status, headers, config){
            $scope.discussion = data;
            $http.get(_apiUrl+'stars/'+data.reply_id).
                success(function(data){
                    $scope.star = data;
                    if(Object.keys(data).length == 0){
                        $scope.empty = 1;
                    }else{
                        $scope.empty = 0;
                    }
                    console.log($scope.empty);
                });
            $http.get(_apiUrl+'reply/'+$routeParams.id+'?page='+$routeParams.page).
                success(function(data, status, headers, config){
                    $scope.replies = data;
                    $scope.page = parseInt($routeParams.page);
                    $scope.id = $routeParams.id;

                });
        });

    $scope.submit = function(form){
        angular.element('.loading').fadeIn();
        angular.element('.button-red').css('display','none');
        $http.post(_apiUrl+'reply', {'id': $routeParams.id, 'user_id': $rootScope.userId, 'body': form.comment}).
            success(function(data, status, headers, config){
                if($routeParams.page == 1){
                    $route.reload();
                    angular.element('.loading').fadeOut();
                }else{
                    $location.path('/discussions/'+$routeParams.id+'/'+1);
                }

            }).
            error(function(data, status,headers, config){
                console.log(data);
            });
    };
});

soyjoyControllers.controller('discussionSearchController', function($scope, $routeParams, $http, $location, $window){
    $http.get(_apiUrl+'search/discussion/'+$routeParams.category+'/'+$routeParams.query).
        success(function(data){
            $scope.discussions = [data];
        });
    $http.get(_apiUrl+'discussioncategory').success(function(data){
        $scope.options = data;
    });

    $scope.search = function(form){
        $location.path('/search/discussion/'+form.category.id_category+'/'+form.query);
    };
    $scope.submit = function(form){
        $http.post(_apiUrl+'ask', {question: form.question, user_id: $rootScope.userId, doctor_id: $routeParams.doctorid}).
            success(function(data, headers, config, status){
                $route.reload();
            });
    }

    $scope.askTrigger = function(){
        angular.element('.form-submit-answer').css('display', 'block');
        angular.element('.ask-button').css('display', 'none');
    }

    $scope.closeForm = function(){
        angular.element('.form-submit-answer').css('display', 'none');
        angular.element('.ask-button').css('display', 'block');
    }
})

soyjoyControllers.controller('userRegisterController', function($scope, $http, $route, $location, $rootScope, $window){
    $scope.master = {};
    $rootScope.pageClass = 'register-page';

    $scope.update = function(user){
        console.log(user);
        user['regid'] = window.localStorage.getItem('regid');
        $http.post(_apiUrl+'auth/app', user).
            success(function(data, status, headers, config){
                window.localStorage.setItem('userId', data.id);
                if(data.token){
                    window.localStorage.setItem('token', data.token);
                    $http.get(_apiUrl+'user/'+data.token).
                        success(function(data, headers, config, status){
                            window.localStorage.setItem('userId', data.id);
                        });
                    $location.url('/');
                }else{
                    var error = '';
                    $.each(data.error, function(index, value) {
                        error += '<li>'+value+'</li>';
                    });
                    angular.element('.register-error').html(error);
                }
            }).error(function(data, status, headers, config){
                console.log(data);
            });
    }
});

soyjoyControllers.controller('userLoginController', function($scope, $http, $location, $rootScope, $route){

    $http.get(_apiUrl+'mobile/logout').success(function(data){
        localStorage.removeItem('token');
        localStorage.removeItem('userId');
        localStorage.clear();
    })
    $scope.master = {};
    $rootScope.pageClass = 'login-page';
    $scope.update = function(user){
        $http.post(_apiUrl+'mobile/login', {email : user.email, password: user.password}).
            success(function(data, status, headers, config){
                console.log(data);
                if(data.status == 1){
                    window.localStorage.setItem('token', data.token);
                    $http.get(_apiUrl+'user/'+data.token).
                        success(function(data, headers, config, status){
                            window.localStorage.setItem('userId', data.id);
                        });
                    $location.path('/');
                }else{
                    alert(data.message);

                }

            }).error(function(data, status, headers, config){
                console.log(data);
            });
    }
});

soyjoyControllers.controller('userForgetController', function($scope, $http, $rootScope, $location){
    $rootScope.pageClass = 'login-page';
    $scope.update = function(form){
        angular.element('.btn-soyjoy').css('display', 'none');
        angular.element('.loading').fadeIn();
        $http.post(_apiUrl+'mobile/forget', {'email' : form.email}).
            success(function(data){
                alert(data.message);
                if(data.status == 0) angular.element('.btn-soyjoy').css('display', 'block'); angular.element('.loading').fadeOut();
            }).error(function(data){
                console.log(data);
            })
    }
});

soyjoyControllers.controller('chatListController', function($scope, $http, $rootScope, $routeParams){
    $rootScope.pageClass = 'ask-page';
    $http.get(_apiUrl+'doctors?page='+$routeParams.page).
        success(function(data, headers, config, status){
            $scope.doctors = data;
            $scope.assets = _assets;
            $scope.page = parseInt($routeParams.page);
        });
});

soyjoyControllers.controller('chatController', function($scope, $http, $location, $routeParams, $rootScope, $cordovaCamera, $cordovaFileTransfer){


    $scope.master = {};
    var doctorId = $routeParams.doctorId;

    $scope.doctorid = doctorId;

    $http.get(_apiUrl+'doctors/'+doctorId).
        success(function(data){
            $scope.dravatar = data.picture;
            //Firebase
            // CREATE A REFERENCE TO FIREBASE
            var messagesRef = new Firebase('https://devdiabetes.firebaseio.com/user/'+doctorId+'/'+$rootScope.userId);

            // REGISTER DOM ELEMENTS
            var messageField = angular.element('#messageInput');
            var nameField = angular.element('#nameInput');
            var messageList = angular.element('#chat-wrap');

            // LISTEN FOR KEYPRESS EVENT

            $scope.send = function(){
                $http.get(_apiUrl+'chat/room/'+doctorId+'/'+$rootScope.userId).
                    success(function(data){
                        //success create room
                    });
                var message = messageField.val();
                //SAVE DATA TO FIREBASE AND EMPTY FIELD
                if(message !== ''){
                    messagesRef.push({text:message, doctor_id: doctorId, user_id: $rootScope.userId, type : 'user'});
                    messageField.val('');
                    window.scrollTo(0,document.body.scrollHeight);
                }
            }

            $scope.getPicture = function() {
                var options = {
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                };
                $cordovaCamera.getPicture(options).then(function(imageURI) {
                    var option = {};
                    $cordovaFileTransfer.upload(_apiUrl+'upload', imageURI, option)
                        .then(function(result) {
                            var fileName = result.response;
                            messagesRef.push({text:'<img src="'+_assets+fileName+'" class="chat-file"/>', doctor_id: doctorId, user_id: $rootScope.userId, type : 'user'});
                        }, function(err) {
                            alert(result)
                        }, function (progress) {
                            // constant progress updates
                        });

                }, function(err) {
                    alert(err);
                });

            }

            messageField.keypress(function (e) {
                if (e.keyCode == 13) {
                    $scope.send();
                }
            });

            // Add a callback that is triggered for each chat message.
            messagesRef.limitToLast(10).on('child_added', function (snapshot) {
                //GET DATA
                var data = snapshot.val();
                var message = data.text;

                //CREATE ELEMENTS MESSAGE & SANITIZE TEXT
                //var messageElement = angular.element('<li class="'+data.type+'">');
                //var nameElement = angular.element("<strong class='chat-list'>"+message+"</strong>");
                $http.get(_apiUrl+'profile/'+$rootScope.token).
                    success(function(result){
                        if(result.avatar == null){
                            if(data.type == 'user'){

                                var chatContent = '<li class="'+data.type+'"><img src="'+_assets+'default-profile-picture.png" class="avatar"/><span class="msg">'+message+'</span></li>';
                            }else{
                                var chatContent = '<li class="'+data.type+'"><img style="border-radius: 100%" src="'+_assets+$scope.dravatar+'" class="avatar"/><span class="msg">'+message+'</span></li>';
                            }

                            messageList.append(chatContent);

                            //SCROLL TO BOTTOM OF MESSAGE LIST
                            messageList[0].scrollTop = messageList[0].scrollHeight;
                        }else{
                            if(data.type == 'user'){

                                var chatContent = '<li class="'+data.type+'"><img src="'+_assets+result.avatar+'" class="avatar"/><span class="msg">'+message+'</span></li>';
                            }else{
                                var chatContent = '<li class="'+data.type+'"><img style="border-radius: 100%" src="'+_assets+$scope.dravatar+'" class="avatar"/><span class="msg">'+message+'</span></li>';
                            }

                            messageList.append(chatContent);

                            //SCROLL TO BOTTOM OF MESSAGE LIST
                            messageList[0].scrollTop = messageList[0].scrollHeight;
                        }
                    })

            });
            //End Firebase


        })


});



soyjoyControllers.controller('askController', function($scope, $rootScope, $http, $routeParams, $route, $cordovaCamera, $cordovaFileTransfer){
    $rootScope.pageClass = 'ask-page';
    $scope.asset = _assets;
    $scope.attachFile = '';
    $http.get(_apiUrl+'doctors/'+$routeParams.doctorid).
        success(function(data, headers, config, status){
            $scope.doctor = data;
            console.log(data);
        });
    $http.get(_apiUrl+'asks/'+$rootScope.userId+'/'+$routeParams.doctorid).
        success(function(data, headers, config, status){
            $scope.asks = data;
        });

    $scope.getPicture = function() {
        var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        };
        $cordovaCamera.getPicture(options).then(function(imageURI) {
            var option = {};
            angular.element('.loading').fadeIn();
            $cordovaFileTransfer.upload(_apiUrl+'upload', imageURI, option)
                .then(function(result) {
                    angular.element('.loading').fadeOut();
                    var fileName = result.response;
                    $scope.attachFile = '<img src="'+_assets+fileName+'"/>';
                }, function(err) {
                    alert(result)
                }, function (progress) {
                    // constant progress updates
                });

        }, function(err) {
            alert(err);
        });

    }
    $scope.submit = function(form){
        angular.element('.loading').fadeIn();
        angular.element('.button-red').css('display', 'none');
        $http.post(_apiUrl+'ask', {question: $scope.attachFile+' '+form.question, user_id: $rootScope.userId, doctor_id: $routeParams.doctorid}).
            success(function(data, headers, config, status){
               $route.reload();
                angular.element('.loading').fadeOut();
            }).error(function(data){
                console.log(data);
            })
    }

    $scope.askTrigger = function(){
        angular.element('.form-submit-answer').css('display', 'block');
        angular.element('.ask-button').css('display', 'none');
    }

    $scope.closeForm = function(){
        angular.element('.form-submit-answer').css('display', 'none');
        angular.element('.ask-button').css('display', 'block');
    }
});

soyjoyControllers.controller('askDetailController', function($scope, $rootScope, $http, $routeParams, $location){
    $http.get(_apiUrl+'ask/show/'+$routeParams.id).
        success(function(data, headers, config, status){
            $scope.ask = data;
            console.log(data);
        });
});

soyjoyControllers.controller('diabetesBuddy', function($scope){

});


soyjoyControllers.controller('mitosFakta', function($scope, $http){
    $http.get(_apiUrl+'misc/myth').
        success(function(data){
            $scope.myInterval = 9999999;
            $scope.myths = data;
            $scope.assets = _assets;
        })
});

soyjoyControllers.controller('alfaController', function($scope, $http){
    $http.get(_apiUrl+'products').
        success(function(data){
            $scope.myInterval = 999999;
            $scope.products = data;
            $scope.assets = _assets;
            $scope.externalLink = function(){
                window.open($scope.products[0].link, '_blank', 'location=yes');
            }
        })


});



soyjoyControllers.controller('checkupController', function($scope, $http, $rootScope, $location){
    $http.get(_apiUrl+'profile/'+$rootScope.token).
        success(function(data){
            if(data.weight == null){
                $location.path('/checkupedit');
            }else{
                //console.log('ada');
            }
        })
});

soyjoyControllers.controller('checkupeditController', function($scope, $http, $rootScope, $location){

    $scope.update = function(form){
        $http.post(_apiUrl+'update/detail', {id: $rootScope.userId, weight: form.weight, height: form.height, activity: form.activity}).
            success(function(data){
                $location.path('/checkup');
            });
    }
    $http.get(_apiUrl+'profile/'+$rootScope.token).
        success(function(data){
            $scope.form = data;
        });
});

soyjoyControllers.controller('foodRecordController', function($scope, $http, $rootScope, $location){
    $http.get(_apiUrl+'calorie/'+$rootScope.userId).
        success(function(data){
            $scope.calorie = data.calorie;
        })

    $http.get(_apiUrl+'foodhistory/'+$rootScope.userId).
        success(function(data){
            $scope.history = data;
            console.log(data);
        })
    $scope.submit = function(form){

        $http.post(_apiUrl+'foodrecord',{
            id: $rootScope.userId,
            makanpagi: angular.element('#makan-pagi').val(),
            snacksiang: angular.element('#snack-siang').val(),
            makansiang: angular.element('#makan-siang').val(),
            snacksore: angular.element('#snack-sore').val(),
            makanmalam: angular.element('#makan-malam').val(),
            year: form.year,
            month: form.month,
            date: form.date
        }).success(function(data){
            if(data.status == 1){
                $location.path('/foodresult');
            }else{
                alert(data.message);
            }
        }).error(function(data){
            console.log(data);
        });
    }
});

soyjoyControllers.controller('foodResultController', function($scope, $http, $rootScope){
    $http.get(_apiUrl+'getfood/'+$rootScope.userId).
        success(function(data){
            console.log(data);
            $scope.foods = data;
            var totalCal = 0;
            angular.forEach(data, function(value){
                totalCal += parseInt(value.calorie);
            })
            $scope.totalCal = totalCal;
        })
    $http.get(_apiUrl+'calorie/'+$rootScope.userId).
        success(function(data){
            $scope.calorie = data.calorie;
        })
});


soyjoyControllers.controller('glucoseController', function($scope, $http, $rootScope, $route, $routeParams, $location){
    $scope.update = function(form){
        $http.post(_apiUrl+'glucosepost', {id: $rootScope.userId, glucose: form.glucose, year: form.year, month: form.month, date: form.date }).
            success(function(data){
                if(data.status == 1){
                    $route.reload();
                }else{
                    alert(data.message);
                }
            }).error(function(data){
                console.log(data);
            });
    }

    $http.get(_apiUrl+'glucose/'+$rootScope.userId+'/'+$routeParams.num).
        success(function(data){
            var master = [];

            angular.forEach(data, function(value, key){

                var point = 0;
                var total = value.result.length;
                angular.forEach(value.result, function(v){
                    point += parseInt(v.value)/total;
                })
                if(point == 0){
                    var level = 'none';
                }else if(point < 60){
                    var level = 'low';
                }else if(point < 100){
                    var level = 'normal';
                }else{
                    var level = 'high';
                }
                master.push({'date': value.date, 'value': point, 'class': level, 'link': value.full});
            });
            $scope.date = master;
        });
    $scope.change = function(filter){
        $location.path('/glucoserecord/'+filter);
    }
});

soyjoyControllers.controller('glucoseEditController', function($scope, $http, $routeParams, $rootScope, $location){
    $http.get(_apiUrl+'editglucose/'+$rootScope.userId+'/'+$routeParams.date).
        success(function(data){
            $scope.records = data;
        })

    $scope.update = function(form){
        angular.element('.glucose-value').each(function(index){
            var glucose_value = angular.element(this).val();
            if(glucose_value !== ''){
                $http.post(_apiUrl+'glucoseupdate', {id: angular.element(this).attr('attr-id'), value: glucose_value });
            }

        })
        $location.path('/glucoserecord/7');
    }
});

soyjoyControllers.controller('weightController', function($scope, $http, $rootScope, $route, $location, $routeParams){
    $http.get(_apiUrl+'profile/'+$rootScope.token).
        success(function(data){
            if(data.height == null){
                $location.path('/checkupedit');
            }else{
                $scope.height = data.height;
                $scope.ideal = data.height - 110;
            }
        })

    $scope.update = function(form){
        $http.post(_apiUrl+'weightpost', {id: $rootScope.userId, weight: parseFloat(form.weight), height: parseFloat($scope.height), year: form.year, month: form.month, date: form.date }).
            success(function(data){
                if(data.status == 1){
                    $route.reload();
                }else{
                    alert(data.message);
                    $route.reload();
                }
            }).
            error(function(data){
                console.log(data);
            })
    }

    $http.get(_apiUrl+'weight/'+$rootScope.userId+'/'+$routeParams.num).
        success(function(data){
            var master = [];


            angular.forEach(data, function(value, key){
                var point = 0;

                angular.forEach(value.result, function(v){
                    point = v.bmi;
                })
                if(point == 0){
                    var level = 'none';
                }else if(point < 18.4){
                    var level = 'low';
                }else if(point < 24.9){
                    var level = 'normal';
                }else{
                    var level = 'high';
                }
                master.push({'date': value.date, 'value': point, 'class': level});
            });
            $scope.date = master;
        });
    $scope.change = function(filter){
        $location.path('/weightrecord/'+filter);
    }
});


soyjoyControllers.controller('tensionController', function($scope, $http, $rootScope, $route, $routeParams, $location){
    $http.get(_apiUrl+'tension/'+$rootScope.userId+'/'+$routeParams.num).
        success(function(data){
            var master = [];


            angular.forEach(data, function(value, key){
                var point = 0;

                angular.forEach(value.result, function(v){
                    point = v.map;
                })
                if(point == 0){
                    var level = 'none';
                }else if(point < 70){
                    var level = 'low';
                }else if(point <= 110){
                    var level = 'normal';
                }else{
                    var level = 'high';
                }
                master.push({'date': value.date, 'value': point, 'class': level});
            });
            $scope.date = master;
        });
    $scope.update = function(form){
        $http.post(_apiUrl+'tensionpost', {id: $rootScope.userId, year: form.year, month: form.month, date: form.date, sistolik: parseFloat(form.sistolik), distolik: parseFloat(form.distolik) }).
            success(function(data){
                if(data.status == 1){
                    $route.reload();
                }else{
                    alert(data.message);
                    $route.reload();
                }
            }).
            error(function(data){
                console.log(data);
            })
        console.log(form);
    }

    $scope.change = function(filter){
        $location.path('/tensirecord/'+filter);
    }
});

soyjoyControllers.controller('diabetesController', function($scope, $route){
    $scope.submit = function(form){
        $result = (28.7 * (parseFloat(form.glukosa))-46.7).toFixed(2);
        angular.element('#diabetes-form').css('display','none');
        angular.element('.result-db').css('display', 'block');
        angular.element('.diabetes-result').html($result);
    }

    $scope.reset = function(){
        $route.reload();
    }
});


soyjoyControllers.directive('menuToggle', function(){
    return{
        restrict: 'A',
        link : function(scope, element){
            $(element).on('click', function(){

                $content = angular.element('.menu-ins');
                if ($content.css('display')=='none'){
                    $('.btn-menu img').attr('src', 'img/nav-close.png');
                    $content.slideDown(100);
                }else{
                    $('.btn-menu img').attr('src', 'img/nav-btn.png');
                    $content.slideUp(100);
                };
            })
        }
    }
});

soyjoyControllers.directive('closeMenu', function(){
    return{
        restrict : 'A',
        link : function(scope, element){
            $(element).on('click', function(){
                $('.btn-menu img').attr('src', 'img/nav-btn.png');
                angular.element('.menu-ins').slideUp(100);
            })
        }
    }
})

soyjoyControllers.directive('rightScroll', function(){
    return {
        restrict : 'A',
        link: function(scope, element){
            scope.$watch("date", function(){
                $(element).scrollLeft(99999);
            });

            setTimeout(function(){
                $('.dot.active').each(function(index){
                    console.log($(this).offset());
                })
            }, 500);
        }
    }
})

soyjoyControllers.directive('openComment', function(){
    return{
        restrict : 'A',
        link : function(scope, element){
            $(element).on('click', function(){
                $('.form-submit-answer').css('display', 'block');
                $('.ask-button').css('display', 'none');
            })
        }
    }
})

soyjoyControllers.directive('closeComment', function(){
    return{
        restrict : 'A',
        link : function(scope, element){
            $(element).on('click', function(){
                $('.form-submit-answer').css('display', 'none');
                $('.ask-button').css('display', 'block');
            })
        }
    }
})

soyjoyControllers.directive('likeDiscussion', function($http, $rootScope, $route){
    return{
        restrict : 'A',
        link : function(scope, element, attrs){
            $(element).on('click', function(){

                $http.get(_apiUrl+'discussionlike/'+attrs.likeDiscussion+'/'+$rootScope.userId).
                    success(function(data){
                        $route.reload();
                    })
            })
        }
    }
})

soyjoyControllers.directive('likeArticle', function($http, $rootScope, $route){
    return{
        restrict : 'A',
        link : function(scope, element, attrs){
            $(element).on('click', function(){

                $http.get(_apiUrl+'articlelike/'+attrs.likeArticle+'/'+$rootScope.userId).
                    success(function(data){
                        $route.reload();
                    })
            })
        }
    }
})


soyjoyControllers.directive('switchPage', function(){
    return{
        restrict : 'A',
        link : function(scope, element, attrs){
            $(element).on('click', function(){
                $(element).parent().css('display', 'none');
            })
        }
    }
})

soyjoyControllers.directive('rotateWheel', function(){
    return{
        restrict : 'A',
        link : function(scope, element){
            Draggable.create($(element), {type:"rotation", throwProps:true});
        }
    }
})

soyjoyControllers.directive('mythAnswer', function(){
    return{
        restrict : 'A',
        link : function(scope, element, attrs){
            $(element).on('click', function(){
                if(attrs.mythAnswer == 0){
                    $('#mitos-'+attrs.mythId).find('.answer').html('SALAH');
                    $('#mitos-'+attrs.mythId).find('.text').removeClass('right');
                    $('#mitos-'+attrs.mythId).find('.text').addClass('wrong');
                }else{
                    $('#mitos-'+attrs.mythId).find('.answer').html('BENAR');
                    $('#mitos-'+attrs.mythId).find('.text').removeClass('wrong');
                    $('#mitos-'+attrs.mythId).find('.text').addClass('right');
                }
                $('#mitos-'+attrs.mythId).fadeIn();
            })
        }
    }
})


soyjoyControllers.directive('circleMenu', function(){
    return{
        restrict : 'A',
        link: function(scope, element, attrs){
            angular.element(document).ready(function(){

                $(element).on('click', function(){
                    $('.inner-circle').fadeIn(100);
                    $('.tap-start').fadeOut(100);
                })

                $(element).circleMenu({
                    item_diameter: 47,
                    circle_radius: 80,
                    trigger:'hover',
                    speed: 200,
                    direction: 'top-half',
                    close: function(){
                        $('.inner-circle').fadeOut(100);
                    }
                });
            })

        }
    }
})

soyjoyControllers.directive('compile', ['$compile', function ($compile) {
  return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
          return scope.$eval(attrs.compile);
        },
        function(value) {
          element.html(value);
          $compile(element.contents())(scope);
        }
      )};
}]);

soyjoyControllers.directive('autoComplete', function($http, $rootScope){
    return{
        restrict : 'A',
        link: function(scope, element, attrs){
            switch(attrs.scheduleMode){
                case '1':
                    $http.get(_apiUrl+'foodhistory/'+$rootScope.userId).
                        success(function(data){
                            var arpush = [];
                            if(data.length != 0){
                                if(data.mpagi[0]){
                                    for(i = 0; i < data.mpagi.length; i++){
                                        arpush.push({id : data.mpagi[i].id, name: data.mpagi[i].name});
                                    }

                                }
                            }


                            $(element).tokenInput(_apiUrl+'food', {
                                placeholder: attrs.autoComplete,
                                queryParam : 'q',
                                method: 'GET',
                                tokenLimit: 10,
                                crossDomain: false,
                                prePopulate : arpush
                            });
                        })
                    break;
                case '2':
                    $http.get(_apiUrl+'foodhistory/'+$rootScope.userId).
                        success(function(data){
                            var arpush = [];
                            if(data.length != 0){
                                if(data.ssiang[0]){
                                    for(i = 0; i < data.ssiang.length; i++){
                                        arpush.push({id : data.ssiang[i].id, name: data.ssiang[i].name});
                                    }
                                }
                            }


                            $(element).tokenInput(_apiUrl+'food', {
                                placeholder: attrs.autoComplete,
                                queryParam : 'q',
                                method: 'GET',
                                tokenLimit: 10,
                                crossDomain: false,
                                prePopulate : arpush
                            });
                        })
                    break;
                case '3':
                    $http.get(_apiUrl+'foodhistory/'+$rootScope.userId).
                        success(function(data){
                            var arpush = [];
                            if(data.length != 0){
                                if(data.msiang[0]){
                                    for(i = 0; i < data.msiang.length; i++){
                                        arpush.push({id : data.msiang[i].id, name: data.msiang[i].name});
                                    }
                                }
                            }

                            $(element).tokenInput(_apiUrl+'food', {
                                placeholder: attrs.autoComplete,
                                queryParam : 'q',
                                method: 'GET',
                                tokenLimit: 10,
                                crossDomain: false,
                                prePopulate : arpush
                            });
                        })
                    break;
                case '4':
                    $http.get(_apiUrl+'foodhistory/'+$rootScope.userId).
                        success(function(data){
                            var arpush = [];
                            if(data.length != 0){
                                if(data.ssore[0]){
                                    for(i = 0; i < data.ssore.length; i++){
                                        arpush.push({id : data.ssore[i].id, name: data.ssore[i].name});
                                    }
                                }
                            }

                            $(element).tokenInput(_apiUrl+'food', {
                                placeholder: attrs.autoComplete,
                                queryParam : 'q',
                                method: 'GET',
                                tokenLimit: 10,
                                crossDomain: false,
                                prePopulate : arpush
                            });
                        })
                    break;
                case '5':
                    $http.get(_apiUrl+'foodhistory/'+$rootScope.userId).
                        success(function(data){
                            var arpush = [];
                            if(data.length != 0){
                                if(data.mmalam[0]){
                                    for(i = 0; i < data.mmalam.length; i++){
                                        arpush.push({id : data.mmalam[i].id, name: data.mmalam[i].name});
                                    }
                                }
                            }
                                                        $(element).tokenInput(_apiUrl+'food', {
                                placeholder: attrs.autoComplete,
                                queryParam : 'q',
                                method: 'GET',
                                tokenLimit: 10,
                                crossDomain: false,
                                prePopulate : arpush
                            });
                        })
                    break;
            }

        }
    }
})

soyjoyControllers.filter('htmlToPlaintext', function() {
    return function(text) {
        return String(text).replace(/<[^>]+>/gm, '');
    }
});



